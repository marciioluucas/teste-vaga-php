import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/Login'
import AuthenticatedLayout from '@/components/Authenticated/AuthenticatedLayout'
import Profile from '@/components/Authenticated/Profile'

Vue.use(Router);

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/auth',
            name: 'AuthenticatedLayout',
            component: AuthenticatedLayout,
            meta: {
                requireAuth: true,
            },
            children: [
                {
                    path: 'profile',
                    name: 'Profile',
                    component: Profile
                },
            ]
        },
        {
            path: '*',
            redirect: '/',
        }
    ]
})
router.beforeEach((to, from, next) => {
    let isDelegated = false;

    for (let matched = (to.matched || []), i = matched.length; i--;) {
        const route = matched[i];

        if (route.beforeEnter) {
            isDelegated = true;
            route.beforeEnter(to, from, next);
        }
    }

    !isDelegated && next();

    const lsToken = localStorage.getItem('token');
    if (lsToken && to.fullPath === '/login') {
        next('/auth/profile');
    }
    if (to.matched.some(record => record.meta.requireAuth)) {
        if (!lsToken && to.fullPath !== '/login') {
            next('/login?q=unauthenticated');
        } else {
            next();
        }
    } else {
        next();
    }
});

export default router;