export default {
    map(params) {
        if (typeof params !== 'undefined' && Object.keys(params).length > 0) {
            const arrObjKeys = Object.keys(params);
            let arrToReturn = arrObjKeys.map((key) => {
                return `${key}=${params[key]}`;
            })
            return `?${arrToReturn.join('&')}`;
        }
        return '';
    }
}
