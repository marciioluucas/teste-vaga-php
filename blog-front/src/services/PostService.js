import Params from '../util/MapParams';
import Service from './Service';

const PostService = {
    getPosts(page, filter) {
        return Service.get(`/posts${Params.map(filter)}`)
    },
    delete(id) {
        return Service.delete(`/post/${id}`)
    },
    edit(post) {
        return Service.put(`/post/${post.id}`, post)
    },
    create(post) {
        return Service.post(`/post`, post)
    },
    uploadImage(image) {
        return Service.post('/post/upload-image', image, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });
    }
};

export default PostService;