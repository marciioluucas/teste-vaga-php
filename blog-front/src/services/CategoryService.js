import Params  from '../util/MapParams';
import Service from './Service';
const CategoryService = {
    getCategories(page, filter) {
        return Service.get(`/categories${Params.map(filter)}`)
    },
    delete(id) {
        return Service.delete(`/category/${id}`)
    }
};

export default CategoryService;
