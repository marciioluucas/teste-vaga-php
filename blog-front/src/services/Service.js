import axios from 'axios'
import * as env from "../../config";

const Service = axios.create({
    baseURL: process.env.NODE_ENV === 'production'
        ? env.build.api.url
        : env.dev.api.url,
    headers: localStorage.getItem('token') !== null
        ? {Authorization: localStorage.getItem('token')}
        : {}
});
export default Service