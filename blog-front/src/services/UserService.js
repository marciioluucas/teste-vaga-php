import Service from './Service';

const UserService = {
    login(user) {
        return Service.post('/login', user);
    }
};

export default UserService;