import Home from './Home'
import Login from './Login'
import Post from './Post'
import AuthenticatedLayout from './Authenticated/AuthenticatedLayout'
import Profile from './Authenticated/Profile'
import App from './App'

export default {
    App,
    Home,
    Login,
    Post,
    AuthenticatedLayout,
    Profile
}