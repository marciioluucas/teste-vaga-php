<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\BackEndBlog\User::class, 50)->create()->each(function ($user) {
            $user->posts()->save(factory(BackEndBlog\Post::class)->make());
            $user->categories()->save(factory(BackEndBlog\Category::class)->make());
        });
    }
}
