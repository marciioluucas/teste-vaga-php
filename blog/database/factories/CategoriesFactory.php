<?php

use Faker\Generator as Faker;

$factory->define(BackEndBlog\Category::class, function (Faker $faker) {
    return [
        'title' => $faker->text(12),
        'description' => $faker->text,
    ];
});
