<?php

use Faker\Generator as Faker;

$factory->define(BackEndBlog\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->text(22),
        'description' => $faker->text,
        'image' => $faker->imageUrl(), // secret
    ];
});
