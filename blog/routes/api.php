<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    // ----users----
    Route::patch('user/{id}', 'UserController@update');
    Route::put('user/{id}', 'UserController@update');
    Route::get('users', 'UserController@retreave');
    Route::get('user/{id}', 'UserController@retreaveById');
    Route::delete('user/{id}', 'UserController@delete');
    Route::post('logout', 'AuthController@logout');

    // ----posts----

    Route::patch('post/{id}', 'PostController@update');
    Route::put('post/{id}', 'PostController@update');
    Route::post('post', 'PostController@create');
    Route::post('post/upload-image', 'PostImageController@store');
    Route::get('post/{id}', 'PostController@retreaveById');
    Route::delete('post/{id}', 'PostController@delete');

     // ----category----

    Route::patch('category/{id}', 'CategoryController@update');
    Route::post('category', 'CategoryController@create');
    Route::put('category/{id}', 'CategoryController@update');
    Route::get('categories', 'CategoryController@retreave');
    Route::get('category/{id}', 'CategoryController@retreaveById');
    Route::delete('category/{id}', 'CategoryController@delete');

});
Route::get('posts', 'PostController@retreave');
Route::post('register','UserController@create');
Route::post('login','Auth\AuthController@login');
Route::get('post/image/{imageName}', 'PostImageController@get');