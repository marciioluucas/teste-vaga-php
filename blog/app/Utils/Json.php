<?php

namespace BackEndBlog\Utils;


class Json
{
    static function encode(string $string) {
        return json_encode($string);
    }

    static function decode(string $string, bool $assoc = true) {
        return json_decode($string, $assoc);
    }
}