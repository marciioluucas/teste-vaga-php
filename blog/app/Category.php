<?php

namespace BackEndBlog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{

    use SoftDeletes;


    protected $fillable = [
        'title', 'status', 'description', 'user_id'
    ];

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'posts_has_categories');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
