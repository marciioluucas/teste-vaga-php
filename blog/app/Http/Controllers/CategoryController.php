<?php

namespace BackEndBlog\Http\Controllers;

use BackEndBlog\Post;
use BackEndBlog\Category;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CategoryController extends Controller implements IController
{
    //
    public function create(Request $req)
    {
        try {
            $req->validate([
                'title' => 'required|string',
                'description' => 'required|string',
            ]);
            $data = $req->all();
            return Category::create([
                'title' => $data['title'],
                'description' => $data['description'],
                'user_id' => Auth::user()->id
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function update(Request $req)
    {
        $req->validate([
            'title' => 'string',
            'description' => 'string',
        ]);
        //req->user
        try {
            $data = $req->all();
            $id = $req->route('id');
            if ($id == null) throw new Exception('ID invalido');
            $user = Category::find($id);
            foreach ($data as $key => $value) {
                $user->$key = $value;
            }

            $user->user_id = $req->user()['id'];
            $user->save();
            return $user;
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function retreave(Request $req)
    {
        try {
            $user = new Category();
            $filters = $req->all();
            foreach ($filters as $filter => $key) {
                $user = $user->where($filter, 'like', $key . "%");
            }
            return $user->paginate(10, ['*'], 'page', array_key_exists('page', $filters) ? $filters['page'] : 1);

        } catch (\Exception $e) {
            throw new \Exception($e);
        }
    }

    /**
     * @param Request $req
     * @return mixed
     * @throws \Exception
     */
    public function retreaveById(Request $req)
    {
        $id = $req->route('id');
        if ($id == null) throw new \Exception('Id invalido');
        return Category::find($id);
    }

    public function delete(Request $req)
    {
        $id = $req->route('id');
        if ($id == null) throw new \Exception('Id invalido');
        return Category::destroy($id);

    }
}
