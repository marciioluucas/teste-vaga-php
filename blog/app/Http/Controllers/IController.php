<?php
/**
 * Created by PhpStorm.
 * User: marci
 * Date: 08/12/2018
 * Time: 09:56
 */

namespace BackEndBlog\Http\Controllers;


use Illuminate\Http\Request;

interface IController
{
    public function create(Request $request);
    public function update(Request $request);
    public function retreave(Request $request);
    public function retreaveById(Request $request);
    public function delete(Request $request);
}