<?php

namespace BackEndBlog\Http\Controllers;

use BackEndBlog\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller implements IController
{
    //
    public function create(Request $req)
    {
        $req->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|unique:users',
            'password' => 'required|string',
        ]);
        try {
            $data = $req->all();
            return User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function update(Request $req)
    {
        $req->validate([
            'name' => 'string|max:255',
            'email' => 'string',
            'password' => 'string',
        ]);
        //req->user
        try {
            $data = $req->all();
            $id = $req->route('id');
            if ($id == null) throw new Exception('ID invalido');
            $user = User::find($id);

            //Adicionar valores aos atributos dinamicamente.
            foreach ($data as $key => $value) {
                if ($key !== 'password') $user->$key = $value;
                else $user->$key = Hash::make($value);
            }
            $user->save();
            return $user;
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function retreave(Request $req)
    {
        try {
            $user = new User();
            $filters = $req->all();
            foreach ($filters as $filter => $key) {
                $user = $user->where($filter, 'like', $key . "%");
            }
            return $user->paginate(10, ['*'], 'page', array_key_exists('page', $filters) ? $filters['page'] : 1);

        } catch (\Exception $e) {
            throw new \Exception($e);
        }
    }

    /**
     * @param Request $req
     * @return mixed
     * @throws \Exception
     */
    public function retreaveById(Request $req)
    {
        $id = $req->route('id');
        if ($id == null) throw new \Exception('Id invalido');
        return User::find($id);
    }

    public function delete(Request $req)
    {
        $id = $req->route('id');
        if ($id == null) throw new \Exception('Id invalido');
        return User::destroy($id);

    }

    public function messages()
    {
        return [
            'name.required' => 'O nome e requirido'
        ];
    }
}
