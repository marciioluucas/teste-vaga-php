<?php

namespace BackEndBlog\Http\Controllers;

use BackEndBlog\Category;
use BackEndBlog\Post;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PostController extends Controller implements IController
{
    //
    public function create(Request $req)
    {
        $req->validate([
            'title' => 'required|string',
            'description' => 'required|string',
            'image' => 'string',
        ]);
        try {
            $data = $req->all();
            $post = Post::create([
                'title' => $data['title'],
                'description' => $data['description'],
                'image' => array_key_exists('image', $data) ? $data['image'] : null,
                'user_id' =>  Auth::user()->id
            ]);
            foreach ($data['categories'] as $categoryId) {
                $category = Category::find($categoryId);
                Post::find($post['id'])->categories()->attach($category);
            }

            return $post;
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
        return null;
    }

    public function update(Request $req)
    {
        $req->validate([
            'description' => 'string',
            'image' => 'string',
            'categories' => 'array'
        ]);

        try {
            $data = $req->all();
            $id = $req->route('id');
            if ($id == null) throw new Exception('ID invalido');
            $post = Post::find($id);

            foreach ($data as $key => $value) {
                if ($key == 'categories') continue;
                $post->$key = $value;
            }

            $post->categories()->detach();
            foreach ($data['categories'] as $categoryId) {
                $category = Category::find($categoryId);
                Post::find($id)->categories()->attach($category);
            }
            $post->save();
            return Post::where('id', $id)->with('categories')->first();
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function retreave(Request $req)
    {
        try {
            $post = new Post();
            $filters = $req->all();
            foreach ($filters as $filter => $key) {
                $post = $post->where($filter, 'like', $key . "%");
            }
            return $post
                ->with('categories')
                ->paginate(5, ['*'], 'page', array_key_exists('page', $filters) ? $filters['page'] : 1);

        } catch (\Exception $e) {
            throw new \Exception($e);
        }
    }

    /**
     * @param Request $req
     * @return mixed
     * @throws \Exception
     */
    public function retreaveById(Request $req)
    {
        $id = $req->route('id');
        if ($id == null) throw new \Exception('Id invalido');
        $post = Post::find($id);
        return $post != null ? $post->with('categories')->first() : $post;
    }

    /**
     * @param Request $req
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function delete(Request $req)
    {
        $id = $req->route('id');
        if ($id == null) throw new \Exception('Id invalido');
        if (!Post::find($id)->where('user_id', $req->user()['id']))
            throw new Exception('Voce não pode excluír este post');
        $message = Post::destroy($id) ? 'Post excluído com sucesso!' : 'Não foi possível excluir este post';
        return response()->json(['message' => $message]);
    }
}
