<?php

namespace BackEndBlog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Intervention\Image\ImageManagerStatic as Image;

class PostImageController extends Controller
{
    public function store(Request $request)
    {
        $name = 'post-image-' . md5('post' . time());
        $path = $request->file('image')->storeAs(
            'posts', $name
        );

        Image::make(base_path() . '/storage/app/' . $path)->encode('jpg')
            ->save(base_path() . '/storage/app/' . $path . '.jpg', 75);
        return response()->json(['image' => 'http://' . $request->getHttpHost() . '/api/post/image/' . $name]);
    }

    public function get(Request $request)
    {
        $headers = [
            'Content-Type', 'image/jpeg',
            'Content-Disposition' => 'attachment; filename=' . $request->route('imageName'),
        ];
        $path = base_path() . '/storage/app/posts/' . $request->route('imageName') . '.jpg';
        $image = Image::make($path)->encode('jpg');
        return Response::make($image, 200)->withHeaders($headers);
    }
}
