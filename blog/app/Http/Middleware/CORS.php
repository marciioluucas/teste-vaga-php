<?php
namespace BackEndBlog\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Response;

class CORS {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        header("Access-Control-Allow-Origin: *");

        $headers = [
            'Access-Control-Allow-Methods'=> 'POST, GET, OPTIONS, PATCH, PUT, DELETE',
            'Access-Control-Allow-Headers'=> '*'
        ];
        if($request->getMethod() == "OPTIONS") {

            return Response::make('OK', 200, $headers);
        }

        $response = $next($request);
        foreach($headers as $key => $value)
            $response->header($key, $value);
        return $response;
    }

}