<?php

namespace BackEndBlog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'title', 'status', 'description', 'image', 'user_id'
    ];

    public function categories() {
        return $this->belongsToMany(Category::class, 'posts_has_categories');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
