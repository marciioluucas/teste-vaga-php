## Introducao

A prova foi feita na intecao de ser RESTFul, fiz um cliente em VueJS para consumí-la


## Instruções

Tenha certeza que voce tem o php, mysql, composer node e npm instalados.

> na pasta blog está o back-end em laravel,

> na pasta blog-front esta o projeto em vue

## Comandos

> $ cd blog

> $ composer install

> $ php artisan migrate

> $ php artisan serve

> $ cd ..

> $ cd blog-front

> $ npm install

> $ npm run dev

O front end vai rodar na localhost:8080 enquanto o back rodará na 8000

PS: O cadastro/alteracao/exclusao de categorias nao foram implementados na tela porém estão funcionando no back-end perfeitamente.
PS2: Utilizem das seeds para cadastrar as categorias.
## Obrigado
